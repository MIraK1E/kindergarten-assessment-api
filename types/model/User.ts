declare interface User {
  email: string
  password?: string
  role: string
}

declare enum UserRole {
  ADMIN,
  TEACHER,
  GAURDIAN
}