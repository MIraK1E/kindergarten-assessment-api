declare interface AppLogger {
  stream(): steamLog
  emerg(message: logMessage): void
  alert(message: logMessage): void 
  crit(message: logMessage): void 
  error(message: logMessage): void 
  warning(message: logMessage): void 
  notice(message: logMessage): void 
  info(message: logMessage): void
  debug(message: logMessage): void 
};

declare interface steamLog {
  write(message: string): void
}

declare interface logMessage {
  path?: string,
  message: string,
}