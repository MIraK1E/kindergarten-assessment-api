import { Model } from "mongoose";
import { IUser } from "../model/User";
import bcrypt from 'bcrypt';

export default class UserRepositoryImplement implements UserRepository {

  private UserModel: Model<IUser>;
  private logger: AppLogger;

  constructor(UserModel: Model<IUser>, logger: AppLogger) {
    this.UserModel = UserModel;
    this.logger = logger;
  }

  createUser = async (data: User) => {
    try {
      const salt = await bcrypt.genSalt(12);
      data.password = await bcrypt.hash(data.password, salt);
      const newUser = await new this.UserModel(data).save();
      delete newUser.password;
      this.logger.info({
        message: `create user with data ${JSON.stringify(newUser)}`
      });
      return newUser;
    } catch (e) {
      this.logger.error({
        path: 'UserRepositoryImplement',
        message: e.message
      });
      return false;
    }
  }

}