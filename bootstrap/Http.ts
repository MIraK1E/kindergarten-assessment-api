import express, { Application } from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import morgan from 'morgan';
import router from './../router/index';

export default class HttpServer {

  private httpServer: Application
  private logger: AppLogger;

  constructor(logger: AppLogger) {
    this.httpServer = express();
    this.logger = logger;
    this.setupServer();
    this.startListen();
    this.setUpRoute();
  }

  setupServer = () => {
    this.httpServer.use(express.json());
    this.httpServer.use(cookieParser());
    this.httpServer.use(cors());
    this.httpServer.use(morgan('combined', { stream: this.logger.stream() }))
  }

  setUpRoute = () => {
    router(this.httpServer);
  }

  startListen = () => {
    this.httpServer.listen(8000, () => {
      this.logger.info({
        message: 'server start on port 8000'
      });
    });
  }
}