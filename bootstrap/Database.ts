import mongoose from 'mongoose';

export default class Database {

  private logger: AppLogger;

  constructor(logger: AppLogger) {
    this.logger = logger;
    
  }

  async connectDatabase() {
    try {
      await mongoose.connect('mongodb://localhost/kindergarten-assessment', { useNewUrlParser: true, useUnifiedTopology: true });
      this.logger.info({ message: 'Initiated database'});
    } catch (e) {
      this.logger.error(e);
    }
  }
}
