import winston, { transports, format, Logger, transport } from 'winston';
const CloudWatchTransport  = require('winston-aws-cloudwatch');

class AppLoggerImplement implements AppLogger {

  protected currentLogger: any | Logger;
  protected environment: string;
  protected level: string;

  constructor(label: string, enviroment: string = 'development', level: string = 'debug') {
    this.environment = enviroment;
    this.level = level;
    this.createLogger(label);
  }

  private setupCloudWatchTranspot(): transport {
    return new CloudWatchTransport({
      logGroupName: '',
      logStreamName: '',
      awsConfig: {
        accessKeyId: 'AKIAVOEOTMLR3RMAGK43',
        secretAccessKey: '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',
        region: ''
      },
      formatLog: (item: { level: any; message: any; meta: any; }) => {
        console.log(item);
        return `${item.level}: ${item.message} ${JSON.stringify(item.meta)}`
      }
    });
  }

  private setupConsoleTranspot(label: string): transport {
    return new transports.Console({
      level: this.level,
      format: format.combine(
        format.colorize(),
        format.simple(),
        format.timestamp(),
        format.printf(info => `${label}[${info.level}]: ${info.message}`),
      )
    });
  }

  private setupTransports(label: string): Array<transport> {
    const transports = [
      this.setupConsoleTranspot(label),
    ];
    if (this.environment === 'production') transports.push(this.setupCloudWatchTranspot());
    return transports;
  }

  private setCurentLogger(label: string): void {
    this.currentLogger = winston.loggers.get(label);
  }

  private isLabelAlreadyHasInLoggerMap(label: string): boolean {
    return winston.loggers.has(label);
  }

  private createLogger(label: string = 'default') {
    if (this.isLabelAlreadyHasInLoggerMap(label)) {
      this.setCurentLogger(label);
      return
    }
    winston.loggers.add(label, {
      transports: [...this.setupTransports(label)]
    });
    this.setCurentLogger(label);
  }

  stream() {
    return {
      write: (message: string) => {
        this.currentLogger.info({
          level: 'info',
          message,
        });
      }
    }
  }

  emerg(message: logMessage) {
    this.currentLogger.emerg(message);
  }

  alert(message: logMessage) {
    this.currentLogger.alert(message);
  }

  crit(message: logMessage) {
    this.currentLogger.crit(message);
  }

  error(message: logMessage) {
    this.currentLogger.error(message);
  }

  warning(message: logMessage) {
    this.currentLogger.warning(message);
  }

  notice(message: logMessage) {
    this.currentLogger.notice(message);
  }

  info(message: logMessage) {
    this.currentLogger.info(message);
  }

  debug(message: logMessage) {
    this.currentLogger.debug(message);
  }
}

export default AppLoggerImplement;