declare interface UserRepository {
  createUser(data: User): Promise<User | boolean>;
}