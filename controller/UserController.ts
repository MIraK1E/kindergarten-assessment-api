import { Request, Response } from 'express';

export default class UserController {

  private UserRepository: UserRepository;

  constructor(UserRepository: UserRepository) {
    this.UserRepository = UserRepository;
  }

  postUser = async (request: Request, response: Response) => {
    const newUser = await this.UserRepository.createUser(request.body);
    if (!newUser) return response
      .status(500)
      .send({ error: true, message: 'สร้าง User ไม่สำเร็จ' });
    return response
      .status(200)
      .send({ error: false, message: 'สร้าง User สำเร็จ', result: newUser });
  }

}