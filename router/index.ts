import { Application } from 'express';
import userRouter from './user-router';


export default (app: Application) => {
  app.use('/users', userRouter);
}
