import Http from './Http';
import Database from './Database';
import Logger from './Logger';

const httpLogger = new Logger('http');
new Http(httpLogger);

const databaseLogger = new Logger('database');
new Database(databaseLogger).connectDatabase();
