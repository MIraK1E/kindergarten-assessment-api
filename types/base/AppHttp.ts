declare interface HttpRequest {
  body: { [key: string]: any },
  query: { [key: string]: any },
  param: { [key: string]: any },
}

declare interface HttpResponse {
  status(): void
  send(): void
}