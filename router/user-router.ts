import { Router } from 'express';
import UserController from './../controller/UserController';
import UserRepository from './../repository/UserRepository';
import User from './../model/User';
import Logger from './../bootstrap/Logger';

const router = Router();
const userRepositoryLogger = new Logger('repository:User')
const userRepository = new UserRepository(User, userRepositoryLogger);
const userController = new UserController(userRepository);

router.post('/', userController.postUser);

export default router;
