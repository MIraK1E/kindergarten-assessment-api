import mongoose, { Schema, Document } from 'mongoose';

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['ADMIN', 'TEACHER', 'GUARDIAN']
  }
});

export interface IUser extends User, Document {};

export const User = mongoose.model<IUser>('User', UserSchema);

export default User;